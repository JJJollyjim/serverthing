defmodule IpUtilsTest do
  use ExUnit.Case

  @archbox_ifaddrs {:ok,
                    [
                      {'lo',
                       [
                         flags: [:up, :loopback, :running],
                         hwaddr: [0, 0, 0, 0, 0, 0],
                         addr: {127, 0, 0, 1},
                         netmask: {255, 0, 0, 0},
                         addr: {0, 0, 0, 0, 0, 0, 0, 1},
                         netmask: {65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535}
                       ]},
                      {'enp5s0',
                       [
                         flags: [:up, :broadcast, :running, :multicast],
                         hwaddr: [244, 109, 4, 212, 120, 140],
                         addr: {192, 168, 1, 205},
                         netmask: {255, 255, 255, 0},
                         broadaddr: {192, 168, 1, 255},
                         addr: {192, 168, 1, 206},
                         netmask: {255, 255, 255, 0},
                         broadaddr: {192, 168, 1, 255},
                         addr: {192, 168, 1, 207},
                         netmask: {255, 255, 255, 0},
                         broadaddr: {192, 168, 1, 255},
                         addr: {192, 168, 1, 208},
                         netmask: {255, 255, 255, 0},
                         broadaddr: {192, 168, 1, 255},
                         addr: {192, 168, 1, 209},
                         netmask: {255, 255, 255, 0},
                         broadaddr: {192, 168, 1, 255},
                         addr: {192, 168, 1, 8},
                         netmask: {255, 255, 255, 0},
                         broadaddr: {192, 168, 1, 255},
                         addr: {64932, 51872, 59332, 36864, 63085, 1279, 65236, 30860},
                         netmask: {65535, 65535, 65535, 65535, 0, 0, 0, 0},
                         addr: {64932, 51872, 59332, 36864, 42512, 24272, 29340, 62274},
                         netmask: {65535, 65535, 65535, 65535, 0, 0, 0, 0},
                         addr: {65152, 0, 0, 0, 59134, 60891, 22797, 19179},
                         netmask: {65535, 65535, 65535, 65535, 0, 0, 0, 0}
                       ]},
                      {'docker0',
                       [
                         flags: [:up, :broadcast, :running, :multicast],
                         hwaddr: [2, 66, 194, 238, 158, 99], 
                         addr: {172, 17, 0, 1},
                         netmask: {255, 255, 0, 0},
                         broadaddr: {172, 17, 255, 255},
                         addr: {65152, 0, 0, 0, 49477, 45260, 18535, 5201},
                         netmask: {65535, 65535, 65535, 65535, 0, 0, 0, 0}
                       ]},
                      {'virbr0',
                       [
                         flags: [:up, :broadcast, :running, :multicast],
                         hwaddr: [82, 84, 0, 85, 61, 117],
                         addr: {192, 168, 122, 1},
                         netmask: {255, 255, 255, 0},
                         broadaddr: {192, 168, 122, 255}
                       ]},
                      {'virbr0-nic',
                       [flags: [:broadcast, :multicast], hwaddr: [82, 84, 0, 85, 61, 117]]},
                      {'veth7f7bf51',
                       [
                         flags: [:up, :broadcast, :running, :multicast],
                         hwaddr: [178, 9, 131, 7, 193, 107],
                         addr: {65152, 0, 0, 0, 59925, 30264, 24338, 13422},
                         netmask: {65535, 65535, 65535, 65535, 0, 0, 0, 0}
                       ]}
                    ]}

  test "phone->archbox" do
    res = ServerthingUtils.IP.find_matching_ip({192,168,1,4}, @archbox_ifaddrs)

    valid_choices = MapSet.new([
      {192, 168, 1, 205},
      {192, 168, 1, 206},
      {192, 168, 1, 207},
      {192, 168, 1, 208},
      {192, 168, 1, 209},
      {192, 168, 1, 8},
    ])

    assert MapSet.member?(valid_choices, res)
  end

  test "archbox localhost" do
    res = ServerthingUtils.IP.find_matching_ip({127,0,0,1}, @archbox_ifaddrs)

    valid_choices = MapSet.new([
      {127,0,0,1},
    ])

    assert MapSet.member?(valid_choices, res)
  end

  test "archbox localhostv6" do
    res = ServerthingUtils.IP.find_matching_ip({0,0,0,0,0,0,0,1}, @archbox_ifaddrs)

    valid_choices = MapSet.new([
      {0,0,0,0,0,0,0,1},
    ])

    assert MapSet.member?(valid_choices, res)
  end
end
