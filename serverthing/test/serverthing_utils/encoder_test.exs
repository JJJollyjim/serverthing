defmodule EncoderUtilsTest do
  use ExUnit.Case

  defstruct [:beans_stuff]

  @serverinfo %Serverthing.PublicServerInfo {
    local_address: "http://192.168.1.2:8096",
    wan_address: "https://media.example.com:8096",
    server_name: "mediaserverbox",
    version: "3.5.2.0",
    operating_system: "Linux",
    id: "mediaserverbox"
  }

  @serverinfo_keys MapSet.new([
    "LocalAddress", "WanAddress", "ServerName", "Version", "OperatingSystem", "Id"
  ])

  test "deriving struct" do
    keys = @serverinfo
    |> ServerthingUtils.Encoder.encode
    |> Poison.decode!
    |> Map.keys
    |> MapSet.new

    assert MapSet.equal?(keys, @serverinfo_keys)
  end

  test "non-deriving struct" do
    keys = %EncoderUtilsTest{beans_stuff: 3}
    |> ServerthingUtils.Encoder.encode
    |> Poison.decode!
    |> Map.keys
    |> MapSet.new

    assert MapSet.equal?(keys, MapSet.new(["BeansStuff"]))
  end

  test "map struct" do
    keys = %{beans_stuff: 3}
    |> ServerthingUtils.Encoder.encode
    |> Poison.decode!
    |> Map.keys
    |> MapSet.new

    assert MapSet.equal?(keys, MapSet.new(["BeansStuff"]))
  end

  test "list" do
    keys = [@serverinfo]
    |> ServerthingUtils.Encoder.encode
    |> Poison.decode!
    |> Enum.at(0)
    |> Map.keys
    |> MapSet.new

    assert MapSet.equal?(keys, @serverinfo_keys)
  end
end
