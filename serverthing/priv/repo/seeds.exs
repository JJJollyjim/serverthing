# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Serverthing.Repo.insert!(%Serverthing.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Serverthing.{Repo, Users.User}

Repo.insert!(User.changeset(
  %User{
    enable_auto_login: false, has_password: false, has_configured_password: false, has_configured_easy_password: false, name: "jamie", last_login_date: DateTime.utc_now(), last_activity_date: DateTime.utc_now(),
    configuration: %User.Config{},
    policy: %User.Policy{is_hidden: false},
  }
))
