defmodule Serverthing.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :has_password, :boolean, null: false
      add :password, :string, null: true
      add :has_configured_password, :boolean, null: false
      add :has_configured_easy_password, :boolean, null: false
      add :enable_auto_login, :boolean, null: false
      add :last_login_date, :utc_datetime, null: true
      add :last_activity_date, :utc_datetime, null: true
    end

  end
end
