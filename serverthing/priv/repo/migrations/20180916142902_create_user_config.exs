defmodule Serverthing.Repo.Migrations.CreateUserConfig do
  use Ecto.Migration

  def change do
    create table(:user_configs, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :play_default_audio_track, :boolean, default: false, null: true
      add :display_missing_episodes, :boolean, default: false, null: false
      add :subtitle_mode, :string, default: "Default"
      add :display_collections_view, :boolean, default: false, null: false
      add :enable_local_password, :boolean, default: false, null: false
      add :hide_played_in_latest, :boolean, default: false, null: true
      add :remember_audio_selections, :boolean, default: false, null: true
      add :remember_subtitle_selections, :boolean, default: false, null: true
      add :enable_next_episode_auto_play, :boolean, default: false, null: true
    end

    alter table(:users) do
      add :configuration_id, references(:user_configs, type: :binary_id), null: false
    end

  end
end
