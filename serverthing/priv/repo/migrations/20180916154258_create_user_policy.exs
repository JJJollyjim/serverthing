defmodule Serverthing.Repo.Migrations.CreateUserPolicy do
  use Ecto.Migration

  def change do
    create table(:user_policies, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :is_administrator, :boolean, default: false, null: false
      add :is_hidden, :boolean, default: false, null: false
      add :is_disabled, :boolean, default: false, null: false
      add :enable_user_preference_access, :boolean, default: false, null: false
      add :enable_remote_control_of_other_users, :boolean, default: false, null: false
      add :enable_shared_device_control, :boolean, default: false, null: false
      add :enable_remote_access, :boolean, default: false, null: false
      add :enable_live_tv_management, :boolean, default: false, null: false
      add :enable_live_tv_access, :boolean, default: false, null: false
      add :enable_media_playback, :boolean, default: false, null: false
      add :enable_audio_playback_transcoding, :boolean, default: false, null: false
      add :enable_video_playback_transcoding, :boolean, default: false, null: false
      add :enable_playback_remuxing, :boolean, default: false, null: false
      add :enable_content_deletion, :boolean, default: false, null: false
      add :enable_content_downloading, :boolean, default: false, null: false
      add :enable_sync_transcoding, :boolean, default: false, null: false
      add :enable_media_conversion, :boolean, default: false, null: false
      add :enable_all_devices, :boolean, default: false, null: false
      add :enable_all_channels, :boolean, default: false, null: false
      add :enable_all_folders, :boolean, default: false, null: false
      add :invalid_login_attempt_count, :integer, null: false
      add :enable_public_sharing, :boolean, default: false, null: false
      add :remote_client_bitrate_limit, :integer, null: false
      add :authentication_provider_id, :string, null: false
    end

    alter table(:users) do
      add :policy_id, references(:user_policies, type: :binary_id), null: false
    end

  end
end
