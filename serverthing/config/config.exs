# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :serverthing,
  ecto_repos: [Serverthing.Repo]

# Configures the endpoint
config :serverthing, ServerthingWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8Al3zYuNHHBlz7cGKhXvKRFpjPZy1f9YwJYYoc/toSnvv9IVppVOPJ9r2u/QlHc5",
  render_errors: [view: ServerthingWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Serverthing.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :phoenix,
  format_encoders: [json: ServerthingUtils.Encode]

config :serverthing, :generators,
  migration: true,
  binary_id: true,
  sample_binary_id: "11111111111111111111111111111111" # TODO still generates uuids for some reason?

