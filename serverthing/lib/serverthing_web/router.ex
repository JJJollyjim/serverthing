defmodule ServerthingWeb.Router do
  use ServerthingWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/emby", ServerthingWeb do
    pipe_through :api

    get "/system/info/public", SystemController, :public_info

    get "/branding/configuration", BrandingController, :conf

    get "/users/public", UsersController, :public_users
  end
end
