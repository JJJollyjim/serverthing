defmodule ServerthingWeb.BrandingController do
  use ServerthingWeb, :controller

  def conf(conn, _params) do
    json conn, %{ login_disclaimer: "This server is running ServerThing #{Mix.Project.config[:version]}. Expect nothing to work." }
  end
end
