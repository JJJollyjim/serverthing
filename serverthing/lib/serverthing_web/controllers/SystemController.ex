defmodule ServerthingWeb.SystemController do
  use ServerthingWeb, :controller

  def public_info(conn, _params) do
    json conn, Serverthing.PublicServerInfo.get(conn.remote_ip)
  end
end
