defmodule ServerthingWeb.UsersController do
  use ServerthingWeb, :controller
  import Ecto.Query, only: [from: 2]

  alias Serverthing.{Repo, Users.User}

  def public_users(conn, _params) do
    users = Repo.all(
       from user in User,
        left_join: config in assoc(user, :configuration),
        left_join: policy in assoc(user, :policy),
        where: policy.is_hidden == false,
        preload: [configuration: config, policy: policy]
    )

    json conn, users
  end
end
