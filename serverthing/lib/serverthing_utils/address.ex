defmodule ServerthingUtils.Address do
  def private_address(remote_ip) do
    disc_conf = Application.get_env :serverthing, ServerthingDiscovery, []
    web_conf = Application.get_env :serverthing, ServerthingWeb.Endpoint, []

    my_ip = ServerthingUtils.IP.find_matching_ip remote_ip, :inet.getifaddrs()

    conf_addr = Keyword.get(disc_conf, :local_address)

    https_port =
      web_conf
      |> Keyword.get(:https, [])
      |> Keyword.get(:port)

    http_port =
      web_conf
      |> Keyword.get(:http, [])
      |> Keyword.get(:port)

    cond do
      conf_addr ->
        conf_addr
      https_port ->
        "https://#{:inet.ntoa(my_ip)}:#{https_port}"
      http_port ->
        "http://#{:inet.ntoa(my_ip)}:#{http_port}"
    end
  end

  def public_address() do
    disc_conf = Application.get_env :serverthing, ServerthingDiscovery, []
    web_conf = Application.get_env :serverthing, ServerthingWeb.Endpoint, []

    public_ip = ServerthingUtils.HotPlate.determine_public_ip()

    conf_addr = Keyword.get(disc_conf, :wan_address)

    https_port =
      web_conf
      |> Keyword.get(:https, [])
      |> Keyword.get(:port)

    http_port =
      web_conf
      |> Keyword.get(:http, [])
      |> Keyword.get(:port)

    cond do
      conf_addr ->
        conf_addr
      https_port ->
        "https://#{public_ip}:#{https_port}"
      http_port ->
        "http://#{public_ip}:#{http_port}"
    end
  end
end
