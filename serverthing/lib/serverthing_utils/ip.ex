defmodule ServerthingUtils.IP do
  def ip_matches(ip1, ip2, mask) do
    use Bitwise

    [ip1, ip2, mask]
    |> Stream.zip()
    |> Enum.all?(fn {i1, i2, m} -> (i1 &&& m) == (i2 &&& m) end)
  end

  def find_matching_ip(reqip_tup, {:ok, ifaddrs}) do
    reqip = Tuple.to_list(reqip_tup)

    ifaddrs
    |> Stream.flat_map(fn {_, v} -> v end)
    |> Stream.filter(fn {k, _} -> k == :addr || k == :netmask end)
    |> Stream.map(fn {_, v} -> v end)
    |> Stream.map(&Tuple.to_list/1)
    |> Stream.chunk_every(2)
    |> Stream.filter(fn [ip, _] -> Enum.count(ip) == Enum.count(reqip) end)
    |> Stream.filter(fn [ip, mask] -> ip_matches(reqip, ip, mask) end)
    |> Stream.map(fn [ip, _] -> List.to_tuple(ip) end)
    |> Enum.at(0)
  end
end
