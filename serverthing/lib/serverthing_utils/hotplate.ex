# TODO: stop hitting this site after release, at hotplate's request

defmodule ServerthingUtils.HotPlate do
  use Memoize
  defstruct [:country, :remote_address]

  defmemo get_hotplate(), expires_in: 1000*60*60*24 do
    Application.ensure_all_started(:inets)
    Application.ensure_all_started(:ssl)
    case :httpc.request(:get, {'https://tools.hotplate.co.nz/', [{'user-agent', 'serverthing #{Mix.Project.config[:version]}'}]}, [], []) do
      {:ok, {{_, 200, 'OK'}, _headers, body}} -> Poison.decode!(body, as: %ServerthingUtils.HotPlate{})
      _ -> %ServerthingUtils.HotPlate{}
    end
  end

  def determine_public_ip() do
    get_hotplate().remote_address
  end

  def determine_country() do
    get_hotplate().country
  end
end
