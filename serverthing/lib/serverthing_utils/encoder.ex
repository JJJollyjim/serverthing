# Derived from Poison's Encoder

defmodule ServerthingUtils.Encode do
  def encode_to_iodata!(x) do
    ServerthingUtils.Encoder.encode(x)
  end
end

defprotocol ServerthingUtils.Encoder do
  @fallback_to_any true

  @spec encode(t) :: iodata
  def encode(value)
end

defimpl ServerthingUtils.Encoder, for: List do
  @compile :inline_list_funcs

  def encode([]), do: "[]"

  def encode(list) do
    fun = &[?,, ServerthingUtils.Encoder.encode(&1) | &2]
    [?[, tl(:lists.foldr(fun, [], list)), ?]]
  end
end

defimpl ServerthingUtils.Encoder, for: Map do
  @compile :inline_list_funcs
  @sep_re ~r/([a-z])_([a-z])/

  @compile {:inline, encode_name: 1, fix_name: 1}

  defp encode_name(value) when is_binary(value) do
    fix_name(value)
  end

  defp encode_name(value) do
    case String.Chars.impl_for(value) do
      nil ->
        raise Poison.EncodeError, value: value,
          message: "expected a String.Chars encodable value, got: #{inspect value}"
      impl ->
        fix_name(impl.to_string(value))
    end
  end


  def encode(map) when map_size(map) < 1, do: "{}"

  def encode(map) do
    fun =
      &[
        ?,,
        Poison.Encoder.BitString.encode(encode_name(&1), %{}),
        ?:,
        ServerthingUtils.Encoder.encode(:maps.get(&1, map)) | &2
      ]

    [?{, tl(:lists.foldl(fun, [], :maps.keys(map))), ?}]
  end

  defp fix_name(<<head::binary-size(1), rest::binary>>) do
    first_up = String.upcase(head) <> rest
    Regex.replace(@sep_re, first_up, fn(_, a, b) -> a <> String.upcase(b) end)
  end
end

defimpl ServerthingUtils.Encoder, for: [Range, Stream, MapSet, HashSet] do
  def encode(x) do
    Poison.Encoder.encode(x, %{})
  end
end

defimpl ServerthingUtils.Encoder, for: [Date, Time, NaiveDateTime, DateTime] do
  def encode(x) do
    Poison.Encoder.encode(x, %{})
  end
end

defimpl ServerthingUtils.Encoder, for: Any do
  defmacro __deriving__(module, struct, options) do
    deriving(module, struct, options)
  end

  def deriving(module, _struct, options) do
    only = options[:only]
    except = options[:except]

    extractor =
      cond do
        only ->
          quote(do: Map.take(struct, unquote(only)))

        except ->
          except = [:__struct__ | except]
          quote(do: Map.drop(struct, unquote(except)))

        true ->
          quote(do: :maps.remove(:__struct__, struct))
      end

    quote do
      defimpl ServerthingUtils.Encoder, for: unquote(module) do
        def encode(struct) do
          ServerthingUtils.Encoder.Map.encode(unquote(extractor))
        end
      end
    end
  end

  def encode(%{__struct__: _} = struct) do
    ServerthingUtils.Encoder.Map.encode(Map.from_struct(struct))
  end

  def encode(value) do
    Poison.Encoder.encode(value, %{})
  end
end
