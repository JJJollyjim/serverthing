defmodule Serverthing.Users.User.Config do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {ServerthingUtils.Encoder, except: [:__meta__, :user, :id]}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "user_configs" do
    field :display_collections_view, :boolean, default: false
    field :display_missing_episodes, :boolean, default: false
    field :enable_local_password, :boolean, default: false
    field :enable_next_episode_auto_play, :boolean, default: true
    field :hide_played_in_latest, :boolean, default: true
    field :play_default_audio_track, :boolean, default: true
    field :remember_audio_selections, :boolean, default: true
    field :remember_subtitle_selections, :boolean, default: true
    field :subtitle_mode, :string, default: "Default"

    field :grouped_folders, :any, virtual: true, default: []
    field :ordered_views, :any, virtual: true, default: []
    field :latest_items_excludes, :any, virtual: true, default: []
    field :my_media_excludes, :any, virtual: true, default: []

    has_one :user, Serverthing.Users.User, foreign_key: :configuration_id
  end

  @doc false
  def changeset(user_config, attrs) do
    user_config
    |> cast(attrs, [:play_default_audio_track, :display_missing_episodes, :subtitle_mode, :display_collections_view, :enable_local_password, :hide_played_in_latest, :remember_audio_selections, :remember_subtitle_selections, :enable_next_episode_auto_play])
    |> validate_required([:play_default_audio_track, :display_missing_episodes, :subtitle_mode, :display_collections_view, :enable_local_password, :hide_played_in_latest, :remember_audio_selections, :remember_subtitle_selections, :enable_next_episode_auto_play])
    |> validate_inclusion(:subtitle_mode, ["Default", "Always", "OnlyForced", "None", "Smart"])
  end
end
