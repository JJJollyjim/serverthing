defmodule Serverthing.Users.User.Policy do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {ServerthingUtils.Encoder, except: [:__meta__, :user, :id]}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "user_policies" do
    field :authentication_provider_id, :string, default: "Emby.Server.Implementations.Library.DefaultAuthenticationProvider"
    field :enable_all_channels, :boolean, default: false
    field :enable_all_devices, :boolean, default: false
    field :enable_all_folders, :boolean, default: false
    field :enable_audio_playback_transcoding, :boolean, default: false
    field :enable_content_deletion, :boolean, default: false
    field :enable_content_downloading, :boolean, default: false
    field :enable_live_tv_access, :boolean, default: false
    field :enable_live_tv_management, :boolean, default: false
    field :enable_media_conversion, :boolean, default: false
    field :enable_media_playback, :boolean, default: false
    field :enable_playback_remuxing, :boolean, default: false
    field :enable_public_sharing, :boolean, default: false
    field :enable_remote_access, :boolean, default: false
    field :enable_remote_control_of_other_users, :boolean, default: false
    field :enable_shared_device_control, :boolean, default: false
    field :enable_sync_transcoding, :boolean, default: false
    field :enable_user_preference_access, :boolean, default: false
    field :enable_video_playback_transcoding, :boolean, default: false
    field :invalid_login_attempt_count, :integer, default: 0
    field :is_administrator, :boolean, default: false
    field :is_disabled, :boolean, default: false
    field :is_hidden, :boolean, default: false
    field :remote_client_bitrate_limit, :integer, default: 0

    field :blocked_tags, :any, virtual: true, default: []
    field :enabled_folders, :any, virtual: true, default: []
    field :enabled_devices, :any, virtual: true, default: []
    field :enabled_channels, :any, virtual: true, default: []
    field :access_schedules, :any, virtual: true, default: []
    field :block_unrated_items, :any, virtual: true, default: []
    field :enable_content_deletion_from_folders, :any, virtual: true, default: []

    has_one :user, Serverthing.Users.User
  end

  @doc false
  def changeset(user_policy, attrs) do
    user_policy
    |> cast(attrs, [:is_administrator, :is_hidden, :is_disabled, :enable_user_preference_access, :enable_remote_control_of_other_users, :enable_shared_device_control, :enable_remote_access, :enable_live_tv_management, :enable_live_tv_access, :enable_media_playback, :enable_audio_playback_transcoding, :enable_video_playback_transcoding, :enable_playback_remuxing, :enable_content_deletion, :enable_content_downloading, :enable_sync_transcoding, :enable_media_conversion, :enable_all_devices, :enable_all_channels, :enable_all_folders, :invalid_login_attempt_count, :enable_public_sharing, :remote_client_bitrate_limit, :authentication_provider_id])
    |> validate_required([:is_administrator, :is_hidden, :is_disabled, :enable_user_preference_access, :enable_remote_control_of_other_users, :enable_shared_device_control, :enable_remote_access, :enable_live_tv_management, :enable_live_tv_access, :enable_media_playback, :enable_audio_playback_transcoding, :enable_video_playback_transcoding, :enable_playback_remuxing, :enable_content_deletion, :enable_content_downloading, :enable_sync_transcoding, :enable_media_conversion, :enable_all_devices, :enable_all_channels, :enable_all_folders, :invalid_login_attempt_count, :enable_public_sharing, :remote_client_bitrate_limit, :authentication_provider_id])
    |> validate_inclusion(:authentication_provider_id, ["Emby.Server.Implementations.Library.DefaultAuthenticationProvider"])
  end
end
