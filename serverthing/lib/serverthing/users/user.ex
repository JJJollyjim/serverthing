defmodule Serverthing.Users.User do
  use Ecto.Schema
  use OK.Pipe
  import Ecto.Changeset

  @derive {ServerthingUtils.Encoder, except: [:__meta__, :configuration_id, :policy_id, :password]}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "users" do
    {:ok, host} = :inet.gethostname() ~> IO.iodata_to_binary

    field :enable_auto_login, :boolean
    field :has_configured_easy_password, :boolean
    field :has_configured_password, :boolean
    field :has_password, :boolean
    field :last_activity_date, :utc_datetime
    field :last_login_date, :utc_datetime
    field :name, :string
    field :password, :string
    field :server_id, :any, virtual: true, default: host
    belongs_to :configuration, Serverthing.Users.User.Config
    belongs_to :policy, Serverthing.Users.User.Policy
  end

  @doc false
  def changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:name, :has_password, :password, :has_configured_password, :has_configured_easy_password, :enable_auto_login, :last_login_date, :last_activity_date])
    |> validate_required([:name, :has_password, :has_configured_password, :has_configured_easy_password, :enable_auto_login])
    |> cast_assoc(:configuration, required: true)
    |> cast_assoc(:policy, required: true)
  end
end
