defmodule Serverthing.PublicServerInfo do
  @derive ServerthingUtils.Encoder
  defstruct [:local_address, :wan_address, :server_name, :version, :operating_system, :id]

  use OK.Pipe

  def get(remote_address) do
    {:ok, host} = :inet.gethostname() ~> IO.iodata_to_binary

    %Serverthing.PublicServerInfo {
      local_address: ServerthingUtils.Address.private_address(remote_address),
      wan_address: ServerthingUtils.Address.public_address(),
      server_name: host,
      # TODO do client apps look at this? Can we report our own version, or must we mimic Emby's?
      version: "3.5.2.0",
      operating_system: "Linux",
      id: host
    }
  end
end
