defmodule ServerthingDiscovery do
  use GenServer
  use OK.Pipe
  require Logger

  @listen_port 7359

  def start_link do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(_) do
    Logger.info("Running ServerthingDiscovery on udp/7359")
    :gen_udp.open(@listen_port, [:binary, active: true])
  end

  def handle_info({:udp, _, address, port, data}, socket) do
    Logger.info("Handling discovery request from #{:inet.ntoa(address)}")

    case make_response(address, data) do
      nil -> nil
      resp -> :gen_udp.send(socket, address, port, Poison.encode_to_iodata!(resp))
    end

    {:noreply, socket}
  end

  defp make_response(remote_ip, "who is EmbyServer?") do
    {:ok, host} = :inet.gethostname() ~> IO.iodata_to_binary

    %{
      Name: host,
      Address: ServerthingUtils.Address.private_address(remote_ip),
      Id: host
    }
  end

  defp make_response(address, _) do
    Logger.warn "Received discovery request with invalid payload from #{:inet.ntoa(address)}"
    nil
  end
end
