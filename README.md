# Serverthing

A truly open source implementation of the [Emby](https://emby.media/) media server, aiming for compatibility with Emby's client apps.

## Getting Started

To run the Elixir application:

  * `cd serverthing`
  * Install dependencies with `mix deps.get`
  * Configure your postgres database in `config/dev.exs`
  * Create and migrate your database with `mix ecto.setup`
  * Start the server with `mix phx.server`

Run the testsuite with `mix test`

